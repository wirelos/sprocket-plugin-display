#include "UserInterface.h"

UserInterface::UserInterface(/* SSD1306* _display, */ int _fontSize, int _rowsPerSite)
{
    //display = _display;
    fontSize = _fontSize;
    rowsPerSite = _rowsPerSite;
    cursorPos = -1;
}
void UserInterface::clear(SSD1306 *display)
{
    display->clear();
    cursorPos = -1;
}
void UserInterface::clearLine(SSD1306 *display, int line){
    display->setColor(BLACK);
    display->fillRect(0, line, display->getWidth(), fontSize);
    display->display();
    display->setColor(WHITE);
}
void UserInterface::printLine(SSD1306 *display, String text)
{
    cursorPos = cursorPos == rowsPerSite ? 0 : cursorPos + 1;
    clearLine(display, cursorPos * fontSize);
    display->drawString(0, cursorPos * fontSize, text);
    display->display();
}
void UserInterface::print(SSD1306 *display, String text)
{
    cursorPos = -1;
    display->clear();
    display->setFont(ArialMT_Plain_10);
    display->setTextAlignment(TEXT_ALIGN_LEFT);
    printLine(display, text);
}
void UserInterface::splashScreen(SSD1306 *display, const char *title, const char *subTitle, const char *version, const char *device)
{
    display->clear();
    display->setFont(ArialMT_Plain_16);
    display->drawString(0, 0, title);
    display->setFont(ArialMT_Plain_24);
    display->drawString(0, 16, subTitle);
    display->setFont(ArialMT_Plain_10);
    display->drawString(100, 28, version);
    display->setFont(ArialMT_Plain_10);
    display->drawString(0, 40, "Device:");
    display->drawString(0, 50, device);
    display->display();
    delay(1600);
}
void UserInterface::drawTitle(OLEDDisplay *display, String text) {
    cursorPos = -1;
    display->clear();
    display->setFont(ArialMT_Plain_24);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    display->drawString(display->getWidth() / 2, (display->getHeight() / 2) - fontSize, text);
    display->display();
}