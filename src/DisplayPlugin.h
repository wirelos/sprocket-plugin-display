#ifndef __DISPLAY_PLUGIN__
#define __DISPLAY_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include <vector>
#include <Plugin.h>
#include <utils/print.h>
#include <UserInterface.h>
#include <SSD1306.h>

using namespace std;
using namespace std::placeholders;

struct DisplayConfig
{
  int i2cAddr;
  int sda;
  int scl;
  int fontSize;
  int rowsPerSite;
  int geometry;
};

class DisplayPlugin : public Plugin
{
public:
  DisplayConfig config;
  SSD1306 *display;
  UserInterface *ui;
  int lastInteraction;
  int displayTimeout;
  Task standbyTask;
  DisplayPlugin(DisplayConfig cfg);
  void activate(Scheduler *scheduler);
  void splashScreen(const char *title, const char *subTitle, const char *version, const char *device);
  void checkStandby();
  void interact();
};

#endif