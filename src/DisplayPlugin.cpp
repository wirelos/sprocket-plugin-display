#include "DisplayPlugin.h"

DisplayPlugin::DisplayPlugin(DisplayConfig cfg)
{
    config = cfg;
    // TODO substitute concrete implementation with base class OLEDDisplay
    display = new SSD1306(config.i2cAddr, config.sda, config.scl, (OLEDDISPLAY_GEOMETRY)config.geometry);
    ui = new UserInterface(config.fontSize, config.rowsPerSite);
}
void DisplayPlugin::splashScreen(const char *title, const char *subTitle, const char *version, const char *device)
{
    ui->splashScreen(display, title, subTitle, version, device);
}
void DisplayPlugin::activate(Scheduler *scheduler)
{
    lastInteraction = millis();
    displayTimeout = 10000; // TODO configurable
    display->init();
    
    // user interface
    subscribe("ui/printLine", bind(&UserInterface::printLine, ui, display, _1));
    subscribe("ui/drawTitle", bind(&UserInterface::drawTitle, ui, display, _1));
    subscribe("ui/print", bind(&UserInterface::print, ui, display, _1));
    subscribe("ui/clear", bind(&UserInterface::clear, ui, display));
    
    // interactions
    subscribe("ui/printLine", bind(&DisplayPlugin::interact, this));
    subscribe("ui/print", bind(&DisplayPlugin::interact, this));
    standbyTask.set(TASK_SECOND * 1, TASK_FOREVER, bind(&DisplayPlugin::checkStandby, this));
    scheduler->addTask(standbyTask);
    standbyTask.enable();
    
    PRINT_MSG(Serial, "DISPLAY", "plugin activated");
}

void DisplayPlugin::interact(){
    lastInteraction = millis();
}

void DisplayPlugin::checkStandby()
{
    if (lastInteraction + displayTimeout < millis())
    {
        display->displayOff();
    }
    else
    {
        display->displayOn();
    }
}
