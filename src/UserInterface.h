#ifndef __USER_INTERFACE__
#define __USER_INTERFACE__

#include <SSD1306.h>
class UserInterface {
    private:
        int fontSize;
        int rowsPerSite;
        int cursorPos;
    public:
        UserInterface(int fontSize, int rowsPerSite);
        //void init();
        void splashScreen(SSD1306* display, const char* title, const char* subTitle, const char* version, const char* device);
        void printLine(SSD1306* display, String text);
        void print(SSD1306* display, String text);
        void clear(SSD1306* display);
        void clearLine(SSD1306 *display, int line);
        void drawTitle(OLEDDisplay *display, String text);
};

#endif