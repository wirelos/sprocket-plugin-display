#include "config.h"
#include "Sprocket.h"
#include "DisplayPlugin.h"

Sprocket *sprocket;
DisplayPlugin *displayPlugin;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    displayPlugin = new DisplayPlugin({0x3c, D1, D2, 12, 4, 0}); // FIXME 4 is actually 5 on screen

    sprocket->addPlugin(displayPlugin);
    sprocket->activate();

    displayPlugin->splashScreen("Wibbly", "Wobbly", "0.0.1", "Sprocket");
    sprocket->publish("ui/clear", "");
    sprocket->publish("ui/printLine", "first line");
    sprocket->publish("ui/printLine", "second line");
    sprocket->publish("ui/printLine", "hello");
    sprocket->publish("ui/printLine", "sweety");
    sprocket->publish("ui/printLine", "last line");
    sprocket->publish("ui/printLine", "first line again");
}

void loop()
{
    sprocket->loop();
    yield();
}