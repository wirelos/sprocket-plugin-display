#include "config.h"
#include "WiFiNet.h"
#include "Sprocket.h"
#include "DisplayPlugin.h"

WiFiNet *network;
Sprocket *sprocket;
DisplayPlugin *displayPlugin;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    displayPlugin = new DisplayPlugin({0x3c, D1, D2, 12, 4, 0}); // FIXME 4 is actually 5 on screen

    sprocket->addPlugin(displayPlugin);
    sprocket->activate();

    displayPlugin->splashScreen("Wibbly", "Wobbly", "0.0.1", "Sprocket");
    sprocket->publish("ui/drawTitle", "Cool!");
    //sprocket->publish("ui/printLine", "connecting to");
    //sprocket->publish("ui/printLine", STATION_SSID);

    network = new WiFiNet(
        SPROCKET_MODE,
        STATION_SSID,
        STATION_PASSWORD,
        AP_SSID,
        AP_PASSWORD,
        HOSTNAME,
        CONNECT_TIMEOUT);
    if (network->connect())
    {
        sprocket->publish("ui/clear", "");
        sprocket->publish("ui/print",
            "Heap: " + String(ESP.getFreeHeap()) + "\n" +
            "Host: " + String(HOSTNAME) + "\n" +
            "AP: " + String(STATION_SSID) + "\n" +
            "IP: " + WiFi.localIP().toString() + "\n" +
            "Gateway: " + WiFi.gatewayIP().toString());
    }
}

void loop()
{
    sprocket->loop();
    yield();
}